namespace InventoryExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HomeItems",
                c => new
                    {
                        HomeItemId = c.Int(nullable: false),
                        Model = c.String(maxLength: 100),
                        SerialNumber = c.String(maxLength: 100),
                        LocationId = c.Int(nullable: false),
                        PurchaseInfoId = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 255),
                        Photo = c.Binary(),
                    })
                .PrimaryKey(t => t.HomeItemId)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .ForeignKey("dbo.PurchaseInfoes", t => t.HomeItemId)
                .Index(t => t.HomeItemId)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.LocationId);
            
            CreateTable(
                "dbo.PurchaseInfoes",
                c => new
                    {
                        PurchaseInfoId = c.Int(nullable: false, identity: true),
                        When = c.DateTime(),
                        Where = c.String(maxLength: 255),
                        Warranty = c.String(maxLength: 255),
                        Price = c.Double(),
                    })
                .PrimaryKey(t => t.PurchaseInfoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HomeItems", "HomeItemId", "dbo.PurchaseInfoes");
            DropForeignKey("dbo.HomeItems", "LocationId", "dbo.Locations");
            DropIndex("dbo.HomeItems", new[] { "LocationId" });
            DropIndex("dbo.HomeItems", new[] { "HomeItemId" });
            DropTable("dbo.PurchaseInfoes");
            DropTable("dbo.Locations");
            DropTable("dbo.HomeItems");
        }
    }
}
