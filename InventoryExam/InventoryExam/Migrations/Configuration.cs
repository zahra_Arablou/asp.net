﻿namespace InventoryExam.Migrations
{
    using InventoryExam.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InventoryExam.Context.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(InventoryExam.Context.ApplicationDbContext context)
        {
            context.Locations.AddOrUpdate(l => l.Name,
                new Location()
                {
                    Name = "Home Office"
                },
                new Location()
                {
                    Name = "Family Room"
                },
                new Location()
                {
                    Name = "Living Room"
                },
                new Location()
                {
                    Name = "Kitchen"
                },
                new Location()
                {
                    Name = "Master Bedroom"
                },
                new Location()
                {
                    Name = "Bedroom Two"
                },
                new Location()
                {
                    Name = "Bedroom Three"
                },
                new Location()
                {
                    Name = "Bathrooms"
                },
                new Location()
                {
                    Name = "Garage"
                },
                new Location()
                {
                    Name = "Attic"
                },
                new Location()
                {
                    Name = "Basement"
                },
                new Location()
                {
                    Name = "Other"
                }
           );
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
