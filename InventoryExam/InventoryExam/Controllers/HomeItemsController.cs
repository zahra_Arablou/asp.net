﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InventoryExam.Context;
using InventoryExam.Models;
using InventoryExam.ViewModel;
using InventoryExam.Helpers;
using PagedList;

namespace InventoryExam.Controllers
{
    public class HomeItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
   

        // GET: HomeItems

        public ActionResult Index(string searchString,int? page)
        {
            ViewBag.SearchString = searchString;
            var homeItems = db.HomeItems.Include(h => h.Location).Include(h => h.PurchaseInfo).OrderBy(h => h.Description).AsQueryable();
            if (!string.IsNullOrEmpty(searchString))
                homeItems = homeItems.Where(s => s.Description.Contains(searchString));

            int pageSize = 1;
            int pageNumber = (page ?? 1);
            var data = homeItems.ToPagedList(pageNumber, pageSize);


            return View(data);

            //var homeItems = db.HomeItems.Include(h => h.Location).Include(h => h.PurchaseInfo);


            //return View(db.HomeItems.ToList());


            //ViewBag.SearchCriteria = searchString;
            //if (!string.IsNullOrEmpty(searchString))
            //    return View(db.HomeItems.OrderByDescending(x => x.Description).Where(s => s.Description.Contains(searchString)).ToList());
            //else


        }

        // GET: HomeItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeItem homeItem = db.HomeItems.Find(id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            return View(homeItem);
        }

        // GET: HomeItems/Create
        public ActionResult Create()
        {
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name");
            ViewBag.HomeItemId = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "Where");
            return View();
        }

        // POST: HomeItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HomeItemViewModel homeItemVM)
        {
            if (ModelState.IsValid)
            {
                var homeItem = new HomeItem();
                homeItem.LocationId = homeItemVM.LocationId;

                homeItem.Description = homeItemVM.Description;
                homeItem.Location = homeItemVM.Location;
                homeItem.Model = homeItemVM.Model;
                homeItem.SerialNumber = homeItemVM.SerialNumber;
                PurchaseInfo purchaseInfo = new PurchaseInfo();
                purchaseInfo.When = homeItemVM.When;
                purchaseInfo.Where = homeItemVM.Where;
                purchaseInfo.Warranty = homeItemVM.Warranty;
                purchaseInfo.Price = homeItemVM.Price;
                homeItem.PurchaseInfo = purchaseInfo;

                if (homeItemVM.Photo != null)

                    homeItem.Photo = ImageConverter.ByteArrayFromPostedFile(homeItemVM.Photo);

                db.HomeItems.Add(homeItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name", homeItemVM.LocationId);
            ViewBag.HomeItemId = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "Where", homeItemVM.HomeItemId);
            return View(homeItemVM);
        }

        // GET: HomeItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeItem homeItem = db.HomeItems.Find(id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name", homeItem.LocationId);
            ViewBag.HomeItemId = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "Where", homeItem.HomeItemId);
            return View(homeItem);
        }

        // POST: HomeItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HomeItemId,Model,SerialNumber,LocationId,PurchaseInfoId,Description,Photo")] HomeItem homeItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(homeItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name", homeItem.LocationId);
            ViewBag.HomeItemId = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "Where", homeItem.HomeItemId);
            return View(homeItem);
        }

        // GET: HomeItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeItem homeItem = db.HomeItems.Find(id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            return View(homeItem);
        }

        // POST: HomeItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HomeItem homeItem = db.HomeItems.Find(id);
            db.HomeItems.Remove(homeItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
