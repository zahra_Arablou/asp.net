﻿using InventoryExam.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace InventoryExam.Context
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<HomeItem>HomeItems { get; set; }
        public DbSet<PurchaseInfo> PurchaseInfos { get; set; }
        public DbSet<Location>Locations { get; set; }
        public ApplicationDbContext():base("DefaultConnection")
        {

        }
    }
}