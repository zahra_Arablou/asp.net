﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCBlog.Models
{
    public class Comment
    {   [Key]
        public int Id { get; set; }

        public string Content { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        [StringLength(255)]
        public string UserFullName { get; set; }

        public int PostId { get; set; }

        public bool IsPublished { get; set; }

    }
}