﻿using MVCBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCBlog.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var posts = db.Posts.Where(p => p.PostedOn.HasValue).OrderByDescending(p => p.PostedOn).ToList();
            return View(posts);

          
        }



    }
}

