﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBlog.ViewModel
{
    public class CreatePostViewModel
    {
        
       
        public string Title { get; set; }

        public string Content { get; set; }
       
        public DateTime? PostedOn { get; set; }
     


    }
}