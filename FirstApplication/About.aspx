﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="FirstApplication.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
     <h2><%: Title %></h2>
    <table>
        <tr>
            <td>
                <asp:ListBox ID="lstSource" runat="server" Height="138px" Width="250px" SelectionMode="Multiple">
                    <asp:ListItem>Pizza</asp:ListItem>
                    <asp:ListItem>Hamburger</asp:ListItem>
                    <asp:ListItem>French Fries</asp:ListItem>
                    <asp:ListItem>Quesadilla</asp:ListItem>
                </asp:ListBox></td>
            <td>
                <asp:Button ID="btnAllRight" runat="server" Text="&gt;&gt;" Width="50px" OnClick="btnAllRight_Click" /><br />
                <asp:Button ID="btnSelectedRight" runat="server" Text="&gt; " Width="50px" OnClick="btnSelectedRight_Click" /><br />
                <asp:Button ID="btnSelectedLeft" runat="server" Text="&lt; " Width="50px" OnClick="btnSelectedLeft_Click" /><br />
                <asp:Button ID="btnAllLeft" runat="server" Text="&lt;&lt;" Width="50px" OnClick="btnAllLeft_Click" />
            </td>
            <td>
                <asp:ListBox ID="lstDestination" runat="server" Height="134px" Width="250px" SelectionMode="Multiple"></asp:ListBox></td>
        </tr>
    </table>
    <h3>Your application description page.</h3>
    <p>Use this area to provide additional information.<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    </p>
    Name:<asp:TextBox ID="txtName" runat="server" AutoPostBack="True" OnTextChanged="txtName_TextChanged" ></asp:TextBox>
    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />

    <br />
    <br />
    <asp:Label ID="lblResult" runat="server"></asp:Label>
    <br />
    Gender:<asp:DropDownList ID="ddlGender" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGender_SelectedIndexChanged">
        <asp:ListItem>Male</asp:ListItem>
        <asp:ListItem>Female</asp:ListItem>
        <asp:ListItem>Other</asp:ListItem>
    </asp:DropDownList>
    <br />

</asp:Content>
