﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication1_2.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <table>
   <tr>
        <td> 
            <asp:Label ID="lblFirstName" runat="server" Text="First Name"></asp:Label>
          
        </td>
        <td>  
            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="erroFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First name is required" Font-Bold="True" ForeColor="Red"></asp:RequiredFieldValidator>

        </td>
      
    </tr>
    <tr>
        <td> 
            <asp:Label ID="lblLastName" runat="server" Text="Last Name"></asp:Label>

        </td>
        <td> 
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="errLastName" runat="server" ErrorMessage="Last Name can not be empty" ControlToValidate="txtLastName" Font-Bold="True" ForeColor="Red" ></asp:RequiredFieldValidator>

        </td>
    </tr>
    <tr>
        <td>

        </td>
        <td>
          
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
            <br />
        </td>
    </tr>
  
</table>
      <asp:Label ID="lblResult" runat="server"></asp:Label>
</asp:Content>
