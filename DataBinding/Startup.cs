﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DataBinding.Startup))]
namespace DataBinding
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
