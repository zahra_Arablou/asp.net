namespace DataBinding.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataBinding.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataBinding.Models.ApplicationDbContext context)
        {
            context.Products.AddOrUpdate(
                new Models.Product() { Name = "Paper", Description = "" },
                new Models.Product() { Name = "NoteBook", Description = "" },
                 new Models.Product() { Name = "Pencil", Description = "Package of 12 pencils" }
                 );
            context.SaveChanges();
        }
    }
}
