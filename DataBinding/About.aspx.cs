﻿using DataBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DataBinding
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                BindGrid();
        }
        private void  BindGrid()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            grvProducts.DataSource = db.Products.ToList();
            grvProducts.DataBind();

        }
    }
}