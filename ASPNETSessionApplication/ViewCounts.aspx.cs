﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPNETSessionApplication
{
    public partial class ViewCounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Application["ViewCount"] == null)
                {
                    Application["ViewCount"] = 1;
                }
                else
                {
                    Application["ViewCount"] = int.Parse(Application["ViewCount"].ToString()) + 1;
                }
                lblViewCount.Text =string.Format("Page: {0:N}", Application["ViewCount"]);
            }

        }
    }
}