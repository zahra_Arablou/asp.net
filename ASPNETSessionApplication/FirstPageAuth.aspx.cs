﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPNETSessionApplication
{
    public partial class FirstPageAuth : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                lblWelcome.Text = string.Format("Welcome {0} user.",Session["user"].ToString());
            }

        }
    }
}