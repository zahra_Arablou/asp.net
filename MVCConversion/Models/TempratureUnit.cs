﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCConversion.Models
{
    public enum TempratureUnit
    {
        Celsius,
        Fahrenheith,
        Kelvin
    }
}