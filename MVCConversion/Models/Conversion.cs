﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCConversion.Models
{
    public class Conversion
    {
        [Display(Name="From")]
        public TempratureUnit temp1 { get; set; }

        [Display(Name = "To")]
        public TempratureUnit temp2 { get; set; }

        [Display(Name = "To convert")]
        [Required]
        public int tempInput { get; set; }

        public string result { get; set; }

    }
}