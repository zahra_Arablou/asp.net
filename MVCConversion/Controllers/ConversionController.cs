﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCConversion.Models;

namespace MVCConversion.Controllers
{
    public class ConversionController : Controller
    {
        // GET: Conversion
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Conversion conversion )
        {
            double result = 0;
            string symbol = "";
            switch (conversion.temp1)
            {
                case TempratureUnit.Celsius:
                    switch (conversion.temp2)
                    {
                            case TempratureUnit.Fahrenheith:
                            result = (conversion.tempInput *1.8)+32;
                            symbol = "°F";
                            break;
                        case TempratureUnit.Kelvin:
                            result = conversion.tempInput+273.15;
                            symbol = "°K";
                            break;

                    }
                    break;
                case TempratureUnit.Fahrenheith:
                    switch (conversion.temp2)
                    {
                        case TempratureUnit.Celsius:
                            result = (conversion.tempInput - 32) / 1.8;
                            symbol = "°C";
                            break;
                      
                        case TempratureUnit.Kelvin:
                            result = (conversion.tempInput - 32) / 1.8 + 273.15;
                            symbol = "°K";
                            break;

                    }
                    break;
                case TempratureUnit.Kelvin:
                    switch (conversion.temp2)
                    {
                        case TempratureUnit.Celsius:
                            result = conversion.tempInput - 273.15;
                            symbol = "°C";
                            break;
                        case TempratureUnit.Fahrenheith:
                            result = (conversion.tempInput - 273.15) * 1.8 + 32;
                            symbol = "°F";
                            break;

                    }
                    break;
               
            }
            conversion.result = string.Format("The conversion from {0} to {1} is : {2}{3}", conversion.temp1, conversion.temp2, result, symbol);
            return View(conversion);
        }

    }
}