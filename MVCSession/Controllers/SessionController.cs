﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSession.Controllers
{
    public class SessionController : Controller
    {
        // GET: Session
        public ActionResult Index()
        {
            var date = HttpContext.Session["date"];
            var content = new ContentResult();
            content.Content = date.ToString();
            return content;
        }
    }
}