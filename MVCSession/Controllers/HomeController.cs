﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSession.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult GoToSession()
        {
            var now =  DateTime.Now;
            if (HttpContext.Session["date"] == null)
                HttpContext.Session["date"] = now;

            return RedirectToAction("Index","Session");

          
        }
    }
}