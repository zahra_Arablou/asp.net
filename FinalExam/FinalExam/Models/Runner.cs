﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FinalExam.Models
{
    public class Runner
    {
        [Key]
        public int Id { get; set; }
        
        [StringLength(250)]
        public string FirstName { get; set; }

        [StringLength(250)]
        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public Gender Gender { get; set; }

        public string Email { get; set; }

        public string  Telephone{ get; set; }

        public string Address { get; set; }

        public string Postal { get; set; }

        public string CountryId { get; set; }

        public string StateId { get; set; }

        public string  ContactName { get; set; }

        public string ContactTelephone { get; set; }

        public int EventId { get; set; }

        [ForeignKey("EventId")]
        public Event Event { get; set; }



    }
}