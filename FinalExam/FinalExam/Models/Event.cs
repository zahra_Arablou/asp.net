﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalExam.Models
{
    public class Event
    {
        [Key]
        public int EventId { get; set; }

        public DateTime EventDate { get; set; }

        public bool IsClosed { get; set; }

    }
}