﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCInventory.Context;
using MVCInventory.Models;
using MVCInventory.Helpers;

namespace MVCInventory.Controllers
{
    public class InventoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Inventories
        public ActionResult Index(string searchString)
        {
            ViewBag.SearchCriteria = searchString;
            if (!string.IsNullOrEmpty(searchString))
                return View(db.inventories.Where(s => s.Description.Contains(searchString)).ToList());
            else
                return View(db.inventories.ToList());
        }

        // GET: Inventories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // GET: Inventories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inventories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Photo,Description,Location,Model,serialNumber,PerchaseInfo")] InventoryViewModel inventoryVM)
        {
            if (ModelState.IsValid)
            {
                var inventory = new Inventory();

                inventory.Description = inventoryVM.Description;
                inventory.Location = inventoryVM.Location;
                inventory.Model = inventoryVM.Model;
                inventory.serialNumber = inventoryVM.serialNumber;
                inventory.PerchaseInfo = inventoryVM.PerchaseInfo;
                if (inventoryVM.Photo != null)

                    inventory.Photo = ImageConverter.ByteArrayFromPostedFile(inventoryVM.Photo);
               

                db.inventories.Add(inventory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inventoryVM);
        }

        // GET: Inventories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }

            var inventoryVM = new InventoryViewModel();
            inventoryVM.Description = inventory.Description;
            inventoryVM.Location = inventory.Location;
            inventoryVM.Model = inventory.Model;
            inventoryVM.serialNumber = inventory.serialNumber;
            inventoryVM.PerchaseInfo = inventory.PerchaseInfo;

            inventoryVM.PhotoDB = inventory.Photo;

            return View(inventoryVM);
        }

        // POST: Inventories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Photo,Description,Location,Model,serialNumber,PerchaseInfo")] InventoryViewModel inventoryVM)
        {
            if (ModelState.IsValid)
            {
                Inventory inventory = db.inventories.Find(inventoryVM.Id);
                if (inventoryVM != null && inventoryVM.Photo != null)
                    inventory.Photo = ImageConverter.ByteArrayFromPostedFile(inventoryVM.Photo);
                inventory.Description = inventoryVM.Description;
                inventory.Location = inventoryVM.Location;
                inventory.Model = inventoryVM.Model;
                inventory.serialNumber = inventoryVM.serialNumber;
                inventory.PerchaseInfo = inventoryVM.PerchaseInfo;





                db.Entry(inventory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inventoryVM);
        }

        // GET: Inventories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // POST: Inventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inventory inventory = db.inventories.Find(id);
            db.inventories.Remove(inventory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
