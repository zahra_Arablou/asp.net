﻿using MVCInventory.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCInventory.Context
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Inventory> inventories { get; set; }
        public ApplicationDbContext(): base("DefaultConnection")
        {

        }
    }
}