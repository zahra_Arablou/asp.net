﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MVCInventory.Models
{
    public class Inventory
    {
        [Key]
        public int Id { get; set; }

        public byte[] Photo { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public  string  Model { get; set; }

        public  string serialNumber { get; set; }

        public PerchaseInfo PerchaseInfo { get; set; }
        public string  Location { get; set;  }



    }
}