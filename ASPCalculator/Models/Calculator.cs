﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPCalculator.Models
{
    public class Calculator
    {
        [Display(Name="Operand 1")]
        [Range(1,100)]
        public int operand1 { get; set; }

        [Display(Name = "Operand 2")]
        [Range(1, 100)]
        public int operand2 { get; set; }
       
        public double? result { get; set; }

        [Required]
        public Operator Operator { get; set; }

    }
}