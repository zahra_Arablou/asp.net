﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPCalculator.Models;

namespace ASPCalculator.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {

            return View();
        }

      [HttpPost]
      public ActionResult Index(Calculator calculator)
        {
            double result = 0;
            switch(calculator.Operator)
            {
                case Operator.Sum:
                    result = calculator.operand1 + calculator.operand2;
                    break;
                case Operator.Substract:
                    result = calculator.operand1 - calculator.operand2;
                    break;
                case Operator.Multiply:
                    result = calculator.operand1 * calculator.operand2;
                    break;
                case Operator.Divide:
                    result = calculator.operand1 / calculator.operand2;
                    break;

            }

            calculator.result = result;

            return View(calculator);
        }
    }
}