﻿using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using AutoMapper;
using MVCStudentRegistration.Helpers;
using MVCStudentRegistration.infrustructure;
using MVCStudentRegistration.Models;
using MVCStudentRegistration.ViewModel;
using PagedList;

namespace MVCStudentRegistration.Controllers
{
    public class StudentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Students
        public ActionResult Index(int? page)
        {
            var students=db.Students.OrderBy(x => x.Name);
            int pageSize = 2;
            int pageNumber = (page ?? 1);
            var data = students.ToPagedList(pageNumber, pageSize);
            return View(data);
        }

        // GET: Students/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Students/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,EnrollementDate,photo")] StudentViewModel studentVM)
        {
            if (ModelState.IsValid)
            {
                var student = new Student();
                student.Name = studentVM.Name;
                student.EnrollementDate = studentVM.EnrollementDate;
                if (studentVM.photo != null) 
                    student.photo = ImageConverter.ByteArrayFromPostedFile(studentVM.photo);
                db.Students.Add(student);
               
                    db.SaveChanges();//commit into the database
                    return RedirectToAction("Index");
                
               
            }

            return View(studentVM);
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }

            //Mapper.CreateMap<Student, StudentViewModel>().ForMember(x => x.Photo, opt => opt.Ignore());
            //var studentVM = Mapper.Map<StudentViewModel>(student);
            //studentVM.PhotoDB = student.photo;
            return View(student);
           
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,EnrollementDate,photo")] StudentViewModel studentVM)
        {
            if (ModelState.IsValid)
            {
                Student student = db.Students.Find(studentVM.Id);
                if (studentVM != null && studentVM.photo != null)
                    student.photo = ImageConverter.ByteArrayFromPostedFile(studentVM.photo);
                student.Name = studentVM.Name;
                student.EnrollementDate= studentVM.EnrollementDate;
                //db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(studentVM);
        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            Student student = db.Students.Find(id);
            db.Students.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
