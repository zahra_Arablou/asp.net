﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCStudentRegistration.ViewModel
{
    public class StudentViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime EnrollementDate { get; set; }
        public HttpPostedFileBase photo { get; set; }
        public byte[] PhotoDB { get; set; }
    }
}