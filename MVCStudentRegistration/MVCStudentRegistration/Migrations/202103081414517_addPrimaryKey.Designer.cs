// <auto-generated />
namespace MVCStudentRegistration.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addPrimaryKey : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addPrimaryKey));
        
        string IMigrationMetadata.Id
        {
            get { return "202103081414517_addPrimaryKey"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
