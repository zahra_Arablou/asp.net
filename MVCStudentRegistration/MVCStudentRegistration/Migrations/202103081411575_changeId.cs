namespace MVCStudentRegistration.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeId : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Students");
            AlterColumn("dbo.Students", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Students", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Students");
            AlterColumn("dbo.Students", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Students", "Id");
        }
    }
}
